const Vec = require('./vec')

const vec = new Vec( 1 , 2)

//version 1
console.log(vec.plus(new Vec(2,3)));
console.log(vec.minus(new Vec(2,3)));
console.log(new Vec(3, 4).length);
console.log(vec.length);                


// version 2 
// console.log(new Vec(1, 2).plus(new Vec(2, 3)));
// console.log(new Vec(1, 2).minus(new Vec(2, 3)));
// console.log(new Vec(3, 4).length);